package org.datepollsystems.localsif.viewmodel

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.Flow
import org.datepollsystems.localsif.R

class HomeFragmentVM : ViewModel() {

    fun btnStartOnClick(view: View){
        view.
        findNavController().navigate(R.id.action_homeFragment_to_dataTransferStartScreen)
    }
}