package org.datepollsystems.localsif.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import org.datepollsystems.localsif.R
import org.datepollsystems.localsif.databinding.FragmentHomeBinding
import org.koin.android.ext.android.bind


class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root
    }


}