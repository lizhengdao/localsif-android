package org.datepollsystems.localsif

import android.app.Application
import org.datepollsystems.localsif.viewmodel.HomeFragmentVM
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import timber.log.Timber

val appModule = module {
    viewModel<HomeFragmentVM>()
}

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.i("Start app")
        Timber.i("Setup koin di")
        startKoin {
            androidLogger(level = Level.ERROR)
            androidContext(this@App)
            modules(listOf(appModule))
        }
    }
}